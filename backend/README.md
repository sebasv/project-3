# GraphQL backend with NestJS

> Based on the example project: [GraphQL Server Example with NestJS (code-first)](https://github.com/prisma/prisma-examples/tree/latest/typescript/graphql-nestjs)

Built with the following stack:

- [**NestJS**](https://docs.nestjs.com/graphql/quick-start): Web framework for building scalable server-side applications
- [**Prisma Client**](https://www.prisma.io/docs/concepts/components/prisma-client): Databases access (ORM)                  
- [**Prisma Migrate**](https://www.prisma.io/docs/concepts/components/prisma-migrate): Database migrations               
- [**SQLite**](https://www.sqlite.org/index.html): Local, file-based SQL database

GraphQL Playground available at `/graphql`