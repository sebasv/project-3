/*
  Warnings:

  - Added the required column `bestsellersRank` to the `Book` table without a default value. This is not possible if the table is not empty.
  - Added the required column `publicationYear` to the `Book` table without a default value. This is not possible if the table is not empty.
  - Added the required column `rating` to the `Book` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Book" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT NOT NULL,
    "publicationYear" INTEGER NOT NULL,
    "isbn10" TEXT,
    "imageUrl" TEXT,
    "bestsellersRank" INTEGER NOT NULL,
    "rating" REAL NOT NULL
);
INSERT INTO "new_Book" ("id", "imageUrl", "isbn10", "title") SELECT "id", "imageUrl", "isbn10", "title" FROM "Book";
DROP TABLE "Book";
ALTER TABLE "new_Book" RENAME TO "Book";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
