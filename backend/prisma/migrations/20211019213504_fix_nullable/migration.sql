-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Book" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT NOT NULL,
    "publicationYear" INTEGER,
    "isbn10" TEXT,
    "isbn13" TEXT,
    "imageUrl" TEXT,
    "bestsellersRank" INTEGER,
    "rating" REAL
);
INSERT INTO "new_Book" ("bestsellersRank", "id", "imageUrl", "isbn10", "isbn13", "publicationYear", "rating", "title") SELECT "bestsellersRank", "id", "imageUrl", "isbn10", "isbn13", "publicationYear", "rating", "title" FROM "Book";
DROP TABLE "Book";
ALTER TABLE "new_Book" RENAME TO "Book";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
