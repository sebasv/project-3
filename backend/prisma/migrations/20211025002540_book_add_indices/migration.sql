-- CreateIndex
CREATE INDEX "Book_rating_idx" ON "Book"("rating");

-- CreateIndex
CREATE INDEX "Book_bestsellersRank_idx" ON "Book"("bestsellersRank");
