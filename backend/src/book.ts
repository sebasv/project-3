import 'reflect-metadata';
import { ObjectType, Field, Int, Float } from "@nestjs/graphql";
import { Author } from './author';
import { Category } from "./category";
import { Review } from './review';

@ObjectType()
export class Book {
  @Field(() => String)
  id: string;

  @Field(() => String, { nullable: true })
  title?: string | null;

  @Field(() => Int, { nullable: true })
  publicationYear?: number;

  @Field(() => String, { nullable: true })
  isbn10?: string | null;

  @Field(() => String, { nullable: true })
  isbn13?: string | null;

  @Field(() => String, { nullable: true })
  imageUrl?: string | null;

  @Field(() => Int, { nullable: true })
  bestsellersRank?: number;

  @Field(() => Float, { nullable: true })
  rating?: number;

  @Field(() => [Author], { nullable: true })
  authors?: Author[] | null;

  @Field(() => [Category], { nullable: true })
  categories?: Category[] | null;

  @Field(() => [Review], { nullable: true })
  reviews?: Review[] | null;
}
