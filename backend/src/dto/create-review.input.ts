import {Field, InputType} from "@nestjs/graphql";
import {Prisma} from "@prisma/client";
import ReviewUncheckedCreateInput = Prisma.ReviewUncheckedCreateInput;

@InputType()
export class CreateReviewInput implements ReviewUncheckedCreateInput {
    @Field(() => String)
    reviewer: string;

    @Field(() => String)
    bookId: string;

    @Field(() => String)
    text: string;
}
