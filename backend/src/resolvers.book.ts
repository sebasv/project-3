import 'reflect-metadata';
import {
  Resolver,
  Query,
  Args,
  Context,
  InputType,
  Field,
  registerEnumType, Int, ObjectType
} from "@nestjs/graphql";
import { Inject } from '@nestjs/common';
import { Book } from './book';
import { PrismaService } from './prisma.service';

@InputType()
class BookOrderBy {
  // Ordering rule should only specify one of these fields!
  // This is because ordering is specified with
  //   [{field1:SortOrder}, {field2:SortOrder}]
  // to preserve field input order

  @Field(() => SortOrder, { nullable: true })
  bestsellersRank?: SortOrder;

  @Field(() => SortOrder, { nullable: true })
  rating?: SortOrder;
}

@ObjectType()
class BooksOutput {
  @Field(() => [Book], { nullable: true })
  books?: [Book] | null;

  @Field(() => Int)
  totalCount: number;
}

enum SortOrder {
  asc = 'asc',
  desc = 'desc',
}

registerEnumType(SortOrder, {
  name: 'SortOrder',
});

@Resolver(Book)
export class BookResolver {
  constructor(@Inject(PrismaService) private prismaService: PrismaService) {}

  /**
   * Generate 'where' rules for fetching books based on fields ordering
   * Ensures that a book is ignored if  any of the ordering fields are null
   * @param orderBy
   */
  fieldRulesFromBookOrderBy(orderBy: BookOrderBy[]) {
    const fieldRules = [];
    for (const o of orderBy) {
      fieldRules.push({ [Object.keys(o)[0]]: { not: null } });
    }
    return fieldRules;
  }

  @Query(() => BooksOutput, { nullable: true })
  async books(
    @Context() ctx,
    @Args('take') take: number,
    @Args('page', { nullable: true }) page: number,
    @Args('search', { nullable: true }) search: string,
    @Args('category', { nullable: true }) category: string,
    @Args('publicationYear', { nullable: true }) publicationYear: number,
    @Args('orderBy', { nullable: true, type: () => [BookOrderBy] })
    orderBy: BookOrderBy[],
  ): Promise<{
    books: Book[],
    totalCount: number
  }> {
    if (orderBy != null) {
      // Validate orderBy objects
      for (const o of orderBy) {
        if (Object.keys(o).length != 1) {
          throw new Error(
            'Illegal number of fields in BookOrderBy object, should be exactly one',
          );
        }
      }
    }
    const whereOR = [];
    const whereAND = [];
    if (search != null) {
      whereOR.push(
        ...[
          { title: { contains: search } },
          { isbn10: { contains: search } },
          { isbn13: { contains: search } },
          {
            authors: {
              some: {
                name: { contains: search },
              },
            },
          },
        ],
      );
    }
    if (orderBy != null) {
      whereAND.push(...this.fieldRulesFromBookOrderBy(orderBy));
    }
    if (category != null) {
      whereAND.push({
        categories: {
          some: {
            name: { equals: category },
          },
        },
      });
    }
    if (publicationYear != null) {
      whereAND.push({ publicationYear: { equals: publicationYear } });
    }
    const where = {
      OR: whereOR.length > 0 ? whereOR : undefined,
      AND: whereAND.length > 0 ? whereAND : undefined,
    }
    const books = await this.prismaService.book.findMany({
      where: where,
      take: take,
      skip: page ? (page - 1) * take : undefined,
      orderBy: orderBy || undefined,
      include: {
        authors: true,
        categories: true,
        reviews: {
          orderBy: {
            createdAt: "desc"
          }
        },
      },
    });
    return {
      books: books,
      totalCount: await this.prismaService.book.count({ where: where })
    }
  }
}
