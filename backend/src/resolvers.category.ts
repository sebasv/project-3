import 'reflect-metadata';
import { Resolver, Query, Args, Context } from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { Category } from './category';
import { PrismaService } from './prisma.service';

@Resolver(Category)
export class CategoryResolver {
  constructor(@Inject(PrismaService) private prismaService: PrismaService) {}

  @Query(() => [Category], { nullable: true })
  async categories(
    @Context() ctx,
    @Args('search', { nullable: true }) search: string,
    @Args('take', { nullable: true }) take: number,
    @Args('page', { nullable: true }) page: number,
  ): Promise<Category[]> {
    return this.prismaService.category.findMany({
      where: search
        ? {
            OR: [
              { name: { contains: search } },
            ]
          }
        : {},
      take: take,
      skip: page ? (page - 1) * take : undefined,
      orderBy: {
        name: 'asc'
      },
    });
  }
}
