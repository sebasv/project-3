import { Field, ObjectType } from '@nestjs/graphql';
import { Book } from './book';

@ObjectType()
export class Review {
  @Field(() => String)
  id: string;

  @Field(() => String)
  reviewer: string;

  @Field(() => Book, { nullable: true })
  book?: Book | null;

  @Field(() => String)
  text: string;

  @Field(() => Date)
  createdAt: Date;
}
