import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { PrismaService } from './prisma.service';
import { BookResolver } from './resolvers.book';
import { CategoryResolver } from "./resolvers.category";
import { join } from 'path';
import {ReviewResolver} from "./resolvers.review";

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      buildSchemaOptions: { dateScalarMode: 'timestamp' },
    }),
  ],
  controllers: [],
  providers: [PrismaService, BookResolver, CategoryResolver, ReviewResolver],
})
export class AppModule {}
