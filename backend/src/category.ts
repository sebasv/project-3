import 'reflect-metadata';
import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Book } from './book';

@ObjectType()
export class Category {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  name?: string | null;

  @Field(() => [Book], { nullable: true })
  books?: [Book] | null;
}
