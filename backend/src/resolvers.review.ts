import {Review} from "./review";
import {Args, Context, Mutation, Resolver} from "@nestjs/graphql";
import {Inject} from "@nestjs/common";
import {PrismaService} from "./prisma.service";
import {CreateReviewInput} from "./dto/create-review.input";

@Resolver(Review)
export class ReviewResolver {
    constructor(@Inject(PrismaService) private prismaService: PrismaService) {}

    @Mutation(() => Review)
    async createReview(
        @Context() ctx,
        @Args('review') review: CreateReviewInput,
    ): Promise<Review> {
        return this.prismaService.review.create({
            data: review,
        });
    }
}
