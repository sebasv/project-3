# Project 3

## Book Surfer
Book Surfer is a service where you can search and filter through over one million book titles. For each book you can view more detailed information and leave a simple text-based review. It essentially functions like an online bookstore without the store element.

## Dataset
Our dataset is a large collection of books from the online bookstore BookDepository. The dataset was downloaded from
[kaggle](https://www.kaggle.com/sp1thas/book-depository-dataset?select=dataset.csv). Since the original dataset has 28 different columns for the books table, we chose to trim it down a little to only include the columns we actually needed. We also needed to write a few python scripts to get the CSVs on a format the SQLite csv importer could understand. There are still some issues with the dataset, like duplicate categories and a few non-book entries. The bestseller ranks are also a little questionable. However, we deemed these issues as less relevant for the purposes of this project.

## UI
We have several components in our code. They include the Header component, the MainContent component, as well as BookCard, ReviewsList, Search, SearchResultList, and SearchHistory components. When you load the webpage you will be taken straight to the main content of our page, which consists of a search filter and a section that displays search results below. The search filter has a number of things you can filter on, e.g. Title, Author, ISBN10 & ISBN13, as well as "select" of category and year, and lastly "sort by" on bestsellers and ratings. You can view and perform previous searches by clicking on the search history button.

By clicking on a BookCard in the Results section, a Modal will display the full information about a book (including an image), and a section dedicated to reviews. The review section has an "add review" section where the user can supply a name and a review for the book they're viewing. The section underneath displays all the reviews given to the specific book. Our UI mainly consists of Ant Design components, which we chose to use because it gives us consistency and a professional look. They provide reliability, and give us room to make the page more advanced. Some components, like the Header, and the application logo, are designed by us.

## Web accessibility
All the ant design components we have used have built-in aria labels. This type of label is used to define a string that labels the current element in cases where a text label is not visible on the screen. An example where we have this is the 'X' button in the upper right of the BookCard Modal, so that the aria label reads "Close" on this button. So, through the use of aria labels, we help assistive technology like screen readers attach a label to an otherwise anonymous HTML element. We apply this ourselves in the Search History section, where we add an aria label to a <List.Item>-element that is clickable without being a direct link (to perform a search again). We also use alternative text on pictures in the application, and by using this we help e.g. those that are visually impaired understand what content is currently on the screen.

Another example of special accomodation we had to provide for accessibility is the category search field. Unfortunately, the component for searching for and picking options which Ant Design provided is seriously lacking because there is no way to clear the field without using a mouse, since one cannot navigate to the clear-button by pressing the tab key. We finally fixed this by re-implementing the clear-button, whilst working around many limitations with the component. It is now possible to press tab in the category search field to focus the clear button, and then pressing enter or space to clear the field.

We have tested the usability and accessibility of the website with the Screen Reader program on Windows, and inspected for accessibility in the browser inspector.

## Testing
### End-2-end testing
End-2-end testing is performed with a testing framework called [Cypress](https://www.cypress.io/). We have written tests covering the most important user scenarios. This includes book search with filters and sorting, user-generated reviews and search history. In accordance with [Cypress’ best practices](https://docs.cypress.io/guides/references/best-practices#Selecting-Elements), the tests rely on special cypress data-attributes (`data-cy`) to verify the existence of and interact with various HTML DOM elements. This increases the stability of the tests as the code base progresses, as it does not rely on CSS selectors which are prone to change. This does allow some unintended layout changes without failing, which might not be tolerable in some situations. 

The end-2-end tests can be run by simply executing `npm run cypress:open` from the `frontend` directory, and choosing `booksurfer.spec.js` from the list of integration tests in the window that appears.

### Unit testing
Unit testing is performed using the packages @testing-library/react and @testing-library/user-event, running on the Jest testing framework. The tests can be run using ```npm test .``` in the frontend-folder. Since unit tests are intended to test a small slice of code, they must be independent of the backend and database. Therefore, we are mocking Apollo, which means that we fake the results of calling its functions. For example, since the ```ReviewsList```-component calls the API to create a new review, we must fake that API call by supplying the return values manually.

## GraphQL
We have used GraphQL as a query language for our API. It has allowed us to create clear and concise queries for what information we want to retrieve. An example is how we, when retrieving books from the database, don’t want all the information included with a book. Some fields in the dataset are simply not relevant to what information we are trying to display in our application.

## Apollo
We utilized [Apollo Client](https://www.apollographql.com/docs/react/) in the frontend to communicate with the GraphQL API. Apollo also served as a state manager and replacement for Redux by caching requests and supporting [reactive variables](https://www.apollographql.com/docs/react/local-state/local-state-management/#reactive-variables). Request caching on Book Surfer is clear when repeating the same search or navigating between the same pages. The search history module takes advantage of a reactive variable storing search related data.


## Database
We use a SQLite database that houses all our books and reviews. SQLite is a simple database engine that requires very little configuration, which makes it very easy to work with on smaller projects. In addition to this, we used an object relational mapper (ORM) called [Prisma](https://www.prisma.io/), which, through Prisma Client, simplifies database access and increases type safety. It also handles database migrations via Prisma Migrate, which makes changes to the database schema easier to manage across a team. Prisma integrated very well with our other tools, such as GraphQL. This all plays together in the NestJS server-side framework we have used.