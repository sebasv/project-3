export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** `Date` type as integer. Type represents date and time as number of milliseconds from start of UNIX epoch. */
  Timestamp: any;
};

export type Author = {
  __typename?: 'Author';
  books?: Maybe<Array<Book>>;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
};

export type Book = {
  __typename?: 'Book';
  authors?: Maybe<Array<Author>>;
  bestsellersRank?: Maybe<Scalars['Int']>;
  categories?: Maybe<Array<Category>>;
  id: Scalars['String'];
  imageUrl?: Maybe<Scalars['String']>;
  isbn10?: Maybe<Scalars['String']>;
  isbn13?: Maybe<Scalars['String']>;
  publicationYear?: Maybe<Scalars['Int']>;
  rating?: Maybe<Scalars['Float']>;
  reviews?: Maybe<Array<Review>>;
  title?: Maybe<Scalars['String']>;
};

export type BookOrderBy = {
  bestsellersRank?: Maybe<SortOrder>;
  rating?: Maybe<SortOrder>;
};

export type BooksOutput = {
  __typename?: 'BooksOutput';
  books?: Maybe<Array<Book>>;
  totalCount: Scalars['Int'];
};

export type Category = {
  __typename?: 'Category';
  books?: Maybe<Array<Book>>;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
};

export type CreateReviewInput = {
  bookId: Scalars['String'];
  reviewer: Scalars['String'];
  text: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createReview: Review;
};


export type MutationCreateReviewArgs = {
  review: CreateReviewInput;
};

export type Query = {
  __typename?: 'Query';
  books?: Maybe<BooksOutput>;
  categories?: Maybe<Array<Category>>;
};


export type QueryBooksArgs = {
  category?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Array<BookOrderBy>>;
  page?: Maybe<Scalars['Float']>;
  publicationYear?: Maybe<Scalars['Float']>;
  search?: Maybe<Scalars['String']>;
  take: Scalars['Float'];
};


export type QueryCategoriesArgs = {
  page?: Maybe<Scalars['Float']>;
  search?: Maybe<Scalars['String']>;
  take?: Maybe<Scalars['Float']>;
};

export type Review = {
  __typename?: 'Review';
  book?: Maybe<Book>;
  createdAt: Scalars['Timestamp'];
  id: Scalars['String'];
  reviewer: Scalars['String'];
  text: Scalars['String'];
};

export enum SortOrder {
  Asc = 'asc',
  Desc = 'desc'
}
