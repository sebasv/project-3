import {CloseCircleFilled} from "@ant-design/icons";
import React from "react";

export function TabbableClearIcon({onClear}: { onClear: () => void }): React.ReactElement {
    return (
        <CloseCircleFilled tabIndex={0} onKeyDown={e => {
            if (e.key === "Enter" || e.key === " ") {
                onClear();
                // Hide dropdown
                e.currentTarget.blur();
                // Prevent everything else
                e.stopPropagation();
                e.preventDefault();
                return false;
            }
        }}/>
    );
}

export default TabbableClearIcon;
