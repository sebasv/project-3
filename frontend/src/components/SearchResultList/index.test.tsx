import {render, screen} from "@testing-library/react";
import SearchResultList from "../SearchResultList";
import {Book} from "../../generated/graphql";

it('Renders titles', () => {
    const books: Book[] = [
        {id: "1", title: "Test1"},
        {id: "2", title: "Test2"},
    ];
    render(
        <SearchResultList books={books} loading={false}/>
    )
    expect(screen.getByText(/Test1/)).toBeVisible()
    expect(screen.getByText(/Test2/)).toBeVisible()
});

it('Renders all authors', () => {
    const books: Book[] = [
        {id: "2", title: "Test2", authors: [{id: 2, name: "Beatrice"}, {id: 3, name: "Thomas"}]},
    ];
    render(
        <SearchResultList books={books} loading={false}/>
    )
    expect(screen.getByText(/Beatrice/)).toBeVisible()
    expect(screen.getByText(/Thomas/)).toBeVisible()
});

it('Shows empty indicator', () => {
    render(
        <SearchResultList books={[]} loading={false}/>
    )
    expect(screen.getByText("No books matched that search")).toBeVisible();
})

it('Shows loading indicator', () => {
    const books: Book[] = [
        {id: "2", title: "Test2", authors: [{id: 2, name: "Beatrice"}, {id: 3, name: "Thomas"}]},
    ];
    render(
        <SearchResultList books={books} loading={true}/>
    )
    expect(document.getElementsByClassName("ant-spin")[0]).toBeVisible();
    render(
        <SearchResultList books={[]} loading={true}/>
    )
    expect(document.getElementsByClassName("ant-spin")[0]).toBeVisible();
})
