import React from "react";
import BookCard from "../BookCard";
import "./styles.css";
import {Book} from "../../generated/graphql";
import { Empty, Spin } from "antd";
import SearchHistory from "../SearchHistory";

// Function that renders a grid with books in a search result
export default function SearchResultList({books, loading}: {books: Book[], loading: boolean}) {

    return (
        <div className="search-result-list">
            <div className="search-result-description">
                <div>
                    <h2 className="search-result-text">Results</h2>
                </div>
                <SearchHistory />
            </div>
            {loading ?
              <div className={"search-result-spin-container"}>
                <Spin size="large" data-cy={"books-loading-spin"} />
              </div>
              :
              books.length > 0 ?
                  <div className="search-result-content">
                    {books.map(book => <BookCard key={book.id} book={book} />)}
                  </div>
                  :
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}
                         description={"No books matched that search"}
                  />
              }
        </div>
    );
}
