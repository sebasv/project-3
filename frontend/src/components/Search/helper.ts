import {useReactiveVar} from "@apollo/client";
import {OrderByColumn, SearchArgs, searchArgsVar, searchHistoryVar} from "./state";
import React, {useEffect, useState} from "react";
import {BookOrderBy, SortOrder} from "../../generated/graphql";
import debounce from "lodash/debounce";

const DEBOUNCE_TIMEOUT = 500;

// Helper function that makes use of the search parameters in the Search component
export function useSearchParameters() {

    // Use (helper) state for the search parameters
    const searchArgs = useReactiveVar(searchArgsVar);
    const [unsentSearch, setUnsentSearch] = useState<string | undefined>(undefined);
    const [unsentYear, setUnsentYear] = useState<number | undefined>(undefined);
    const [orderByColumn, setOrderByColumnRaw] = useState<OrderByColumn>(
        searchArgs.orderBy?.rating != null ? OrderByColumn.RATING : OrderByColumn.BESTSELLERS_RANK
    );
    const [partialCategorySearch, setPartialCategorySearch]= React.useState<string>("");
    const [categorySearch, setCategorySearch] = React.useState<string | undefined>(undefined);
    const [debouncing, setDebouncing] = React.useState<boolean>(false);
    const debounceRef = React.useRef(0);

    // Append to search history when a different search happens
    useEffect(() => {
        if (
            (searchArgs.search?.length ?? "") > 0
            || (searchArgs.category?.length ?? "") > 0
            || (searchArgs.publicationYear != null)
        ) {
            searchHistoryVar(searchHistoryVar().concat({
                search: searchArgs.search,
                category: searchArgs.category,
                publicationYear: searchArgs.publicationYear
            }));
        }
    }, [searchArgs.search, searchArgs.category, searchArgs.publicationYear])

    // Set the visible search parameters to those used in the new search
    useEffect(() => {
        setUnsentYear(searchArgs.publicationYear)
        setUnsentSearch(searchArgs.search)
        setCategorySearch(searchArgs.category)
    }, [searchArgs]);

    // Wrap setOrderByColumnRaw to update the search args on change
    const setOrderByColumn = (column: OrderByColumn) => {
        let newSort: BookOrderBy | undefined;
        switch (column) {
            case OrderByColumn.RATING:
                newSort = {
                    rating: SortOrder.Desc,
                }
                break;
            case OrderByColumn.BESTSELLERS_RANK:
                newSort = {
                    bestsellersRank: SortOrder.Asc,
                }
                break;
        }
        setOrderByColumnRaw(column ?? OrderByColumn.NONE);
        applyInputtedSearch({orderBy: newSort});
    };

    // "Debounce" updating of category search to only query API after user has stopped typing
    // (based on https://ant.design/components/select/#components-select-demo-select-users)
    const debounceCategorySearch = React.useMemo(() => {
        debounceRef.current += 1;
        const debounceId = debounceRef.current;
        const updateCategorySearch = (search: string) => {
            if (debounceId !== debounceRef.current) {
                return;
            }
            setCategorySearch(search);
            setDebouncing(false);
        };
        return debounce(updateCategorySearch, DEBOUNCE_TIMEOUT);
    }, []);

    // Change the search to use the inputted (unsent) values,
    // with optional overrides for values which always trigger re-search on change (category, sort order).
    function applyInputtedSearch(overrides?: Omit<SearchArgs, "take">) {
        searchArgsVar({
            ...searchArgsVar(),
            search: unsentSearch,
            publicationYear: unsentYear,
            page: 1, // return to first page for each new search
            ...overrides})
    }

    return {
        searchArgs, applyInputtedSearch,
        unsentSearch, setUnsentSearch,
        unsentYear, setUnsentYear,
        orderByColumn, setOrderByColumn,
        categorySearch, setCategorySearch,
        partialCategorySearch, setPartialCategorySearch,
        debouncing, setDebouncing, debounceCategorySearch,
    }
}
