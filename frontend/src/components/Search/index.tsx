import React from "react";
import {Button, Card, Empty, Form, Input, InputNumber, Pagination, Radio, Select, Spin, Typography} from "antd";
import "./styles.css";
import {CalendarOutlined, PieChartOutlined, SearchOutlined, TagOutlined} from "@ant-design/icons";
import {OrderByColumn} from "./state";
import {getBooksQuery, getCategoriesQuery} from "../../api/ApiBase";
import SearchResultList from "../SearchResultList";
import {useQuery} from "@apollo/client";
import {useSearchParameters} from "./helper";
import TabbableClearIcon from "../TabbableClearIcon/TabbableClearIcon";

const { Title } = Typography;

// Function that renders the main search filter container for filtering books on specific titles/years/etc.,
// as well as the search results of a specific search
export default function Search() {

  // Define search parameters and debouncing properties
  const {
    searchArgs, applyInputtedSearch,
    unsentSearch, setUnsentSearch,
    unsentYear, setUnsentYear,
    orderByColumn, setOrderByColumn,
    categorySearch,
    partialCategorySearch, setPartialCategorySearch,
    debouncing, setDebouncing, debounceCategorySearch,
  } = useSearchParameters();

  // Get all books matching the search args with a getBooksQuery
  const booksQuery = useQuery(getBooksQuery, { variables: searchArgs });

  // Get all categories matching the search arg with a getCategoriesQuery
  const categoriesQuery = useQuery(getCategoriesQuery, {
    variables: {
      take: categorySearch !== "" ? 10 : 0,
      search: categorySearch
    }
  });

  return (<>
    <div className="search-container">
      <Card title={
        <Title level={4} style={{ lineHeight: "inherit" }}>
          <SearchOutlined /> Search books
        </Title>
      }>
        <Form>
          <div className="search-input-filters">
            <div className="search-text-input input-filter">
              <TagOutlined className="search-input-icon" />
              <Input placeholder="Title / author / ISBN" allowClear
                     value={unsentSearch ?? ""}
                     onChange={e => setUnsentSearch(e.target.value !== "" ? e.target.value : undefined)}
                     data-cy={"book-search"}
              />
            </div>
            <div className="year-input input-filter">
              <CalendarOutlined className={"search-input-icon"} />
              <InputNumber className="year-input-field" min={0} max={2021} placeholder={"Year"}
                           value={unsentYear} onChange={value => setUnsentYear(value)}
                           data-cy={"book-year-input"}
              />
            </div>
            <div className="category-input input-filter">
              <PieChartOutlined className={"search-input-icon"} />
              <Select<string>
                  className="category-select"
                  data-cy={"book-category-select"}
                  allowClear
                  showSearch
                  placeholder="Category"
                  filterOption={(input, option: any) =>
                      option?.label?.toLowerCase().includes(input.toLowerCase()) ?? false
                  }
                  clearIcon={<TabbableClearIcon onClear={() => {
                      applyInputtedSearch({category: undefined})
                      setPartialCategorySearch("")
                  }}/>}
                  onChange={value => applyInputtedSearch({category: value})}
                  searchValue={partialCategorySearch}
                  onSelect={value => applyInputtedSearch({category: value})}
                  value={categorySearch === "" ? searchArgs.category : categorySearch}
                  onSearch={s => {
                      setPartialCategorySearch(s)
                      setDebouncing(true);
                      debounceCategorySearch(s);
                  }}
                  notFoundContent={debouncing || categoriesQuery.loading ?
                      <div className={"category-select-spin-container"}>
                        <Spin size="small" data-cy={"categories-loading-spin"} />
                      </div>
                      :
                      categorySearch !== "" && <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                      description={"No category matched that search"}
                      />
                  }
                  // See https://ant.design/components/select/#components-select-demo-select-users
                  options={categoriesQuery.data?.categories?.filter(c => c.name != null).map(c => ({
                    value: c.name as string,
                    label: c.name as string,
                    key: c.id
                  })) ?? []}
              >
              </Select>
            </div>
            <div className="sort-by-input input-filter">
              <div className="sort-by-label">
                Sort&nbsp;by
              </div>
              <div>
                <Radio.Group onChange={e => setOrderByColumn(e.target.value)} value={orderByColumn}>
                  <Radio value={OrderByColumn.RATING} data-cy={"book-sort-rating"}>Rating</Radio>
                  <Radio value={OrderByColumn.BESTSELLERS_RANK} data-cy={"book-sort-bestsellers"}>Bestsellers</Radio>
                </Radio.Group>
              </div>
            </div>
            <div className="button-perform-search">
              <Button
                  title="Search"
                  type="primary"
                  htmlType={"submit"}
                  className={"search-button"}
                  aria-label="search"
                  data-cy={"book-search-submit"}
                  onClick={() => applyInputtedSearch()}
              >
                Search
              </Button>
            </div>
          </div>
        </Form>
      </Card>
    </div>
    <SearchResultList books={booksQuery.data?.books?.books ?? []} loading={booksQuery.loading} />
    {!booksQuery.loading && (booksQuery.data?.books?.books ?? []).length > 0 &&
      <div className={"pagination-container"}>
        <Pagination
          defaultPageSize={searchArgs.take}
          total={booksQuery.data?.books?.totalCount}
          current={searchArgs.page}
          onChange={(page, pageSize) => {
            applyInputtedSearch({
              page: page,
              ...(pageSize != null && { take: pageSize })
            });
          }}
        />
      </div>
    }
  </>);
}
