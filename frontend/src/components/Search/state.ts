import {BookOrderBy, SortOrder} from "../../generated/graphql";
import {makeVar} from "@apollo/client";

// Define an enumerator for ordering content in a specific way
export enum OrderByColumn {
    NONE,
    RATING,
    BESTSELLERS_RANK,
}

// Define the types of the search arguments
export interface SearchArgs {
    search?: string,
    publicationYear?: number,
    category?: string,
    orderBy?: BookOrderBy,
    page?: number,
    take: number,
}

export type SearchHistoryEntry = Pick<SearchArgs, "search" | "publicationYear" | "category">;

export const searchHistoryVar = makeVar<SearchHistoryEntry[]>([]);

export const searchArgsVar = makeVar<SearchArgs>({
    take: 25,
    page: 1,
    orderBy: { rating: SortOrder.Desc }
});
