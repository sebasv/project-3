import React from "react";
import "./styles.css";
import logo from "../../book-logo-new.png";


// Function that renders the application header with our logo and a title
export default function Header() {
    return (
        <div className="header">
            <img src={logo} className="header-logo" alt="Book surfer application logo"/>
            <div className="header-text-container">
                <h1 className="header-text">Book Surfer</h1>
            </div>
        </div>
    );
}
