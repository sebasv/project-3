import React from "react";
import Search from '../Search';
import "./styles.css";

// Function that renders the main content in the application, namely a search section with relevant results
export default function MainContent() {
    return (
        <div className="main-content-container">
            <Search/>
        </div>
    );
}
