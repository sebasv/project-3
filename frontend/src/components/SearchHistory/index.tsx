import React, { useState } from "react";
import { searchArgsVar, searchHistoryVar } from "../Search/state";
import { useReactiveVar } from "@apollo/client";
import { Alert, Button, Divider, Drawer, Empty, List, Typography } from "antd";
import "./styles.css";
import { HistoryOutlined } from "@ant-design/icons";


// Function that renders a Drawer with a search history containing search history entries
export default function SearchHistory() {
  // Handle state of a Search History Drawer
  const [visible, setVisible] = useState(false);

  // Logic related to displaying and closing a Drawer
  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  // Use helper state in the Search component to get an object containing search history entries
  const searchHistory = useReactiveVar(searchHistoryVar);

  const { Text } = Typography;

  return (
    <div className="search-history-content">
      <Button onClick={showDrawer} size="small" data-cy={"search-history-button"}>
        <HistoryOutlined /> Search History
      </Button>
      <Drawer className="search-history-drawer" title="Search History" placement="right" onClose={onClose} visible={visible}>
        {searchHistory.length > 0 ?
          <>
            <Alert message="Click on one of the search history entries below to perform the search again" type="info" showIcon />
            <Divider />
            <List
              bordered
              dataSource={searchHistory.slice().reverse()}
              renderItem={(v, i) => (
                <List.Item className="search-history-entry" aria-label="Clickable search history item" data-cy={"search-history-entry"}
                  onClick={
                    () => {
                      searchArgsVar({ ...searchArgsVar(), ...v, page: 1 })
                      onClose()
                    }
                  }

                  >
                  <div key={i}>
                    <Text type="secondary">Search</Text>
                    {v.search &&
                      <><Text type="secondary"> for </Text>{v.search}</>
                    }
                    {v.publicationYear &&
                      <><Text type="secondary"> in year </Text>{v.publicationYear}</>
                    }
                    {v.category &&
                      <><Text type="secondary"> under category </Text>{v.category}</>
                    }

                  </div>
                </List.Item>
              )}
            />
          </>
          :
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={"You haven't searched for anything yet!"} />
        }

      </Drawer>
    </div>
  );
}
