import {Review} from "../../generated/graphql";
import React from "react";
import {List, Typography} from "antd";

// Function that renders a specific review in a list of reviews
function ReviewListItem(review: Review): React.ReactElement {
    return (
        <List.Item key={review.id} style={{whiteSpace: "pre-wrap"}}>
            <div>
                <Typography.Text>{review.text}</Typography.Text>
                <br/>
                <Typography.Text type="secondary">{review.reviewer}, {new Date(review.createdAt).toISOString().slice(0,10)}</Typography.Text>
            </div>
        </List.Item>
    );
}

export default ReviewListItem;
