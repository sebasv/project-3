import { Button, ConfigProvider, Empty, Form, Input, List } from "antd";
import "./styles.css";
import React from "react";
import {Book} from "../../generated/graphql";
import {FormValuesType, useSubmitReview} from "./helper";
import ReviewListItem from "./ReviewListItem";

// Function that renders a form for submitting reviews and a section displaying all submitted reviews
export default function ReviewsList({book}: { book: Book }) {
    const [form] = Form.useForm<FormValuesType>();
    const reviews = book.reviews ?? [];
    const submitReview = useSubmitReview();

    return (
        <div className="reviews-list-container">
            <div className="reviews-list-text">
                <h3>Add review</h3>
            </div>

            <Form
                form={form}
                onFinish={values => {submitReview(book)(values); form.resetFields()}}
                layout="vertical"
            >
                <Form.Item label="Review" name="text" rules={[{required: true, message: "Please write a review"}]}>
                    <Input.TextArea autoSize
                                    placeholder="Enter review..."
                                    autoCapitalize="sentences"
                                    autoComplete="off"
                                    data-cy={"book-review-text-input"}
                    />
                </Form.Item>
                <Form.Item label="Name" name="reviewer" rules={[{required: true, message: "Please write your name"}]}>
                    <Input placeholder="Enter name..."
                           autoCapitalize="words"
                           data-cy={"book-review-reviewer-input"}
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary"
                            htmlType="submit"
                            data-cy={"book-review-submit"}
                    >
                      Submit
                    </Button>
                </Form.Item>
            </Form>

            <div className="reviews-list-text">
                <h3>Reviews</h3>
            </div>

            <div className="reviews-content">
                <ConfigProvider renderEmpty={() => (
                  <Empty
                         description={"No reviews yet, write the first one!"} />
                )}>
                  <List
                    size="small"
                    bordered
                    dataSource={reviews}
                    renderItem={ReviewListItem}
                  />
                </ConfigProvider>
            </div>

        </div>
    );
}
