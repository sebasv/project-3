import {client, createReviewMutation} from "../../api/ApiBase";
import {gql, useMutation} from "@apollo/client";
import {Book, Review} from "../../generated/graphql";

// Define the types of values in a review
export type FormValuesType = Pick<Review, 'text' | 'reviewer'>

// Helper function that creates and updates a review in the UI and database
export function useSubmitReview() {
    const [createReview] = useMutation(createReviewMutation);

    return (book: Book) => (values: FormValuesType) => {
        try {
            createReview({
                variables: {
                    review: {...values, bookId: book.id}
                },
                // Update data live
                update: (cache: typeof client.cache, {data}) => {
                    if (data == null) {
                        return;
                    }
                    cache.writeFragment({
                        data: {
                            ...book,
                            // Add created review to start of book reviews list
                            reviews: [data.createReview].concat(book?.reviews ?? []),
                        },
                        fragment: gql`
                            fragment UpdateBook on Book {
                                reviews
                            }
                        `
                    })
                }
            }).then();
        } catch (e) {
            alert("Failed to create review");
        }
    }
}
