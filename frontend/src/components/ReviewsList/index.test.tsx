import {act, render, screen, waitFor} from "@testing-library/react";
import ReviewsList from "./index";
import {Book, Review} from "../../generated/graphql";
import {MockedProvider, MockedResponse} from "@apollo/client/testing";
import {createReviewMutation, getBooksQuery} from "../../api/ApiBase";
import userEvent from "@testing-library/user-event";
import {useQuery} from "@apollo/client";
import React from "react";

const {type, click} = userEvent;

// Fix for https://github.com/ant-design/ant-design/issues/21096
beforeAll(() => {
    Object.defineProperty(window, 'matchMedia', {
        value: () => {
            return {
                matches: false,
                addListener: () => {
                },
                removeListener: () => {
                }
            };
        }
    })
})

// Wait for the mock to return results by skipping this tick of the event loop.
// Details: https://www.apollographql.com/blog/frontend/testing-apollos-query-component-d575dc642e04/
function tick() {
    return waitFor(() => new Promise(resolve => setTimeout(resolve)))
}

it('Shows review text and names', () => {
    const book: Book = {
        id: "1",
        reviews: [
            {id: "1", reviewer: "Katrine", text: "Loved it!", createdAt: "2021-10-01"},
            {id: "2", reviewer: "Kato", text: "Hated it!", createdAt: "2021-10-01"},
            {id: "3", reviewer: "Caine", text: "Lost it...", createdAt: "2021-10-01"},
        ],
    }
    render(
        <MockedProvider>
            <ReviewsList book={book}/>
        </MockedProvider>
    )
    expect(screen.getByText(/Loved it!/)).toBeVisible();
    expect(screen.getByText(/Hated it!/)).toBeVisible();
    expect(screen.getByText(/Lost it.../)).toBeVisible();
    expect(screen.getByText(/Katrine/)).toBeVisible();
    expect(screen.getByText(/Kato/)).toBeVisible();
    expect(screen.getByText(/Caine/)).toBeVisible();
})

it('Successfully creates review', async () => {
    const review1: Review = {__typename: "Review", id: "1", reviewer: "Katrine", text: "Loved it!", createdAt: "2021-10-01"}
    const review2: Review = {__typename: "Review", id: "2", reviewer: "Kato", text: "Hated it!", createdAt: "2021-10-02"}
    const mockCreateReview: MockedResponse = {
        request: {
            query: createReviewMutation,
            variables: {
                review: {text: "Lost it...", reviewer: "Caine", bookId: "3"}
            }
        },
        result: {
            data: {
                createReview: {id: "3", reviewer: "Caine", text: "Lost it...", bookId: "3", createdAt: "2021-10-03"},
            }
        },
    }
    const book: Book = {
        __typename: "Book",
        id: "3",
        title: "Sherlock Holmes",
        categories: null,
        publicationYear: null,
        authors: null,
        isbn13: null,
        isbn10: null,
        imageUrl: null,
        bestsellersRank: null,
        rating: null,
        reviews: [review1, review2
        ],
    }
    const mockBooksQuery: MockedResponse = {
        request: {
            query: getBooksQuery,
        },
        result: {
            data: {
                books: {
                    books: [book],
                    totalCount: 1,
                }
            }
        }
    }

    function ReviewsListWithMockedData(): React.ReactElement {
        return <ReviewsList book={useQuery(getBooksQuery).data?.books.books[0] ?? {id: "Loading"}}/>
    }
    render(
        <MockedProvider mocks={[mockCreateReview, mockBooksQuery]}>
            <ReviewsListWithMockedData/>
        </MockedProvider>
    )
    
    await tick();

    expect(screen.getByText(/Loved it!/)).toBeVisible();
    expect(screen.getByText(/Hated it!/)).toBeVisible();
    expect(screen.getByText(/Katrine/)).toBeVisible();
    expect(screen.getByText(/Kato/)).toBeVisible();
    await act(async () => {
        // Add some delay between chars to prevent overwriting
        await type(screen.getByPlaceholderText("Enter name..."), "Caine", {delay: 1})
        type(screen.getByPlaceholderText("Enter review..."), "Lost it...")
        click(screen.getByText("Submit"))
    })
    expect(screen.getByText(/Loved it!/)).toBeVisible();
    expect(screen.getByText(/Hated it!/)).toBeVisible();
    expect(screen.getByText(/Katrine/)).toBeVisible();
    expect(screen.getByText(/Kato/)).toBeVisible();
    expect(screen.getByText(/Lost it.../)).toBeVisible();
    expect(screen.getByText(/Caine/)).toBeVisible();
})
