import React, {useState} from 'react';
import {Card, Divider, Empty, Image, Modal} from "antd";
import "./styles.css";
import ReviewsList from '../ReviewsList';
import {Book} from "../../generated/graphql";

const {Meta} = Card;

export default function BookCard({book}: { book: Book }): React.ReactElement {
    // Keeping state of the Book Modal
    const [isModalVisible, setIsModalVisible] = useState(false);

    // Logic related to showing/closing the Modal that displays more information about a book
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    // Format authors and categories so they can be displayed in a nice way in the UI
    const authorsString = book.authors?.map(a => a.name).join(", ") ?? "Unknown";
    const categoriesString = book.categories?.map(c => c.name).join(", ") ?? "Unknown";
    
    // Check that image (book cover) is not null and render Image element
    const image = (preview: boolean) => (
        book?.imageUrl != null ?
        <Image
            alt={"Book cover for " + book.title}
            src={book.imageUrl}
            preview={preview}
        />
        :
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}/>
    );

    return (
        <div className="book-card-container">
            <Card
                hoverable
                onClick={showModal}
                size="small"
                cover={image(false)}
                data-cy={"book-card"}
            >
                <Meta
                    title={book.title}
                    description={authorsString}
                />
            </Card>
            <Modal
                className="book-modal"
                centered
                visible={isModalVisible}
                footer={null}
                title={book.title}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <div className="book-modal-content">
                    <div className="book-information">
                        <div className="book-modal-text">
                            <p><strong>Author{(book.authors?.length ?? 0) > 1 ? "s" : ""}:</strong> {authorsString}</p>
                            <p><strong>Year:</strong> {book.publicationYear ?? "Unknown"}</p>
                            <p><strong>Categor{(book.categories?.length ?? 0) > 1 ? "ies" : "y"}:</strong> {categoriesString}</p>
                        </div>
                        <div className="book-modal-img">
                            {image(true)}
                        </div>
                    </div>
                    <Divider/>
                    <ReviewsList book={book}/>
                </div>
            </Modal>
        </div>
    );
}
