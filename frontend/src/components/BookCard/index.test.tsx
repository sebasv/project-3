import {act, render, screen} from "@testing-library/react";
import BookCard from "./index";
import {Book} from "../../generated/graphql";
import userEvent from "@testing-library/user-event";
import {MockedProvider} from "@apollo/client/testing";

const {click} = userEvent;


// Fix for https://github.com/ant-design/ant-design/issues/21096
beforeAll(() => {
    Object.defineProperty(window, 'matchMedia', {
        value: () => {
            return {
                matches: false,
                addListener: () => {
                },
                removeListener: () => {
                }
            };
        }
    })
})

it('Opening and closing modal', () => {
    const book: Book = {
        id: "1",
        title: "Cool book",
        authors: [
            {id: 1, name: "Bob"},
            {id: 2, name: "Jones"},
        ],
        publicationYear: 1999,
        imageUrl: "https://wikipedia.org/favicon.ico",
        categories: [{id: 1, name: "cool"}, {id: 2, name: "cooler"}],
    }
    render(
        <MockedProvider>
            <BookCard book={book}/>
        </MockedProvider>
    )
    expect(screen.getByText(book.title!)).toBeVisible()
    for (const author of book.authors!) {
        expect(screen.getByText(RegExp(author.name!)))
    }
    expect((screen.getByAltText("Book cover") as HTMLImageElement).src).toBe(book.imageUrl)
    act(() => {
        click(screen.getByAltText("Book cover"))
    })
    expect(screen.getByText(RegExp(book.publicationYear!.toString()))).toBeVisible()
    expect(document.getElementsByClassName("book-modal")[0]).toBeVisible()
    act(() => {
        click(document.querySelector(".ant-modal-close")!)
    })
    expect(document.getElementsByClassName("book-modal")[0]).not.toBeVisible()
    expect(screen.getByText(RegExp(book.publicationYear!.toString()))).not.toBeVisible()
});
