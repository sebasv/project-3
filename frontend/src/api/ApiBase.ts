import { ApolloClient, gql, InMemoryCache, TypedDocumentNode } from "@apollo/client";
import { Book, Category, Review } from "../generated/graphql";

// Define the Apollo client that will be used to perform queries
export const client = new ApolloClient({
  uri: "http://localhost:8000/graphql",
  cache: new InMemoryCache()
});

// Query that gets all relevant information about a book
export const getBooksQuery: TypedDocumentNode<{ books: { totalCount: number, books: Book[] } }> = gql`
    query GetBooks($orderBy: [BookOrderBy!], $publicationYear: Float, $category: String, $search: String,
        $page: Float, $take: Float!) {
        books(orderBy: $orderBy, publicationYear: $publicationYear, category: $category, search: $search,
            page: $page, take: $take) {
            totalCount,
            books {
                id
                title
                authors {
                    id
                    name
                }
                categories {
                    id
                    name
                }
                publicationYear
                imageUrl
                isbn10
                isbn13
                reviews {
                    text
                    reviewer
                    createdAt
                }
            }
        }
    }
`;

// Query that gets all the categories in our dataset
export const getCategoriesQuery: TypedDocumentNode<{ categories: Category[] }> = gql`
    query GetCategories($page: Float, $take: Float!, $search: String) {
        categories(page: $page, take: $take, search: $search) {
            id
            name
        }
    }
`;

export const createReviewMutation: TypedDocumentNode<{ createReview: Review }> = gql`
    mutation createReview($review: CreateReviewInput!) {
        createReview(review: $review) {
            id
            reviewer
            text
            createdAt
        }
    }
`;
