import React from 'react';
import './App.css';
import Header from './components/Header';
import MainContent from './components/MainContent';
import {ApolloProvider} from "@apollo/client";
import {client} from "./api/ApiBase";


// Function that returns the application Header and MainContent, in the appropriate provider
function App() {
    return (
        <div className="App">
            <ApolloProvider client={client}>
                <Header/>
                <MainContent/>
            </ApolloProvider>
        </div>
    );
}

export default App;
