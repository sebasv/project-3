// Helper for using appropriate timeouts when waiting for API responses
const waitForLoadingSpin = (spin) => cy.get(`[data-cy=${spin}]`, { timeout: 60000 }).should("not.exist")

const waitForBooksLoading = () => waitForLoadingSpin("books-loading-spin")
const waitForCategoriesLoading = () => waitForLoadingSpin("categories-loading-spin")

describe('book surfer e2e', () => {
  beforeEach(() => {
    cy.visit('localhost:3000')
  })

  it('searches with filter and sorting', () => {
    cy.get('[data-cy=book-search]').type("Dune")
    cy.get('[data-cy=book-year-input]').type("2020")
    cy.get('[data-cy=book-category-select]').type("Classic")
    waitForCategoriesLoading()
    cy.contains('Classic Books & Novels').click()
    cy.get('[data-cy=book-sort-bestsellers]').focus().click()
    waitForBooksLoading()
    cy.contains('Dune Messiah')
    cy.contains('Children of Dune')
    cy.contains('God Emperor of Dune')
  })

  it('finds a book and posts a review', () => {
    cy.get('[data-cy=book-search]').type("9781782118619")
    cy.get('[data-cy=book-search-submit').click()
    waitForBooksLoading()
    cy.contains("How to Stop Time", {timeout: 20000}).click()
    cy.contains("Author: Matt Haig")
    cy.contains("Year: 2017")
    cy.contains("Category: Contemporary Fiction")
    const reviewText = "Matt Haig has an empathy for the human condition, the light and the dark of it, and he uses" +
      " the full palette to build his excellent stories."
    const reviewer = "Neil"
    cy.get('[data-cy=book-review-text-input]').type(reviewText)
    cy.get('[data-cy=book-review-reviewer-input]').type(reviewer)
    cy.get('[data-cy=book-review-submit').click()
    cy.contains(reviewText)
    cy.contains(`${reviewer}, ${new Date().toISOString().slice(0,10)}`)
    cy.get('[aria-label=Close]').click()
    cy.contains(reviewer).should('not.be.visible')
  })

  it('stores searches in search history', () => {
    const testSearches = [
      {
        title: "Dune",
        year: 2020,
        categoryShort: "Classic",
        category: "Classic Books & Novels"
      },
      {
        title: "Fahrenheit",
        year: 2012,
        categoryShort: "Contemporary",
        category: "Contemporary Fiction"
      },
      {
        title: "Snow Crash",
        year: 2016,
        categoryShort: "Fantasy",
        category: "Fantasy"
      }
    ]

    // Perform test searches
    for (const search of testSearches) {
      cy.get('[data-cy=book-search]').clear().type(search.title)
      cy.get('[data-cy=book-year-input]').clear().type(search.year.toString())
      cy.get('[data-cy=book-category-select]').type(search.categoryShort)
      waitForCategoriesLoading()
      cy.get('[data-cy=book-category-select]').get(`[title='${search.category}']`).click()
    }

    // Open search history drawer
    cy.get('[data-cy=search-history-button]').click()

    // Test if all searches have been saved and presented in reverse chronological order
    const reverseSearches = testSearches.slice().reverse()  // slice to preserve original
    for (let i = 0; i < reverseSearches.length; i++) {
      const search = reverseSearches[i]
      cy.get("[data-cy=search-history-entry]")
          .eq(i)
          .should(
              "contain.text",
              `Search for ${search.title} in year ${search.year} under category ${search.category}`
          )
    }

    // Test if search history entry restores search correctly
    cy.get("[data-cy=search-history-entry]").eq(testSearches.length-1).click()
    const firstSearch = testSearches[0]
    cy.get('[data-cy=book-search]').should("have.value", firstSearch.title)
    cy.get('[data-cy=book-year-input]').should("have.value", firstSearch.year)
    cy.get('[data-cy=book-category-select]').should("have.text", firstSearch.category)
  })

})